// import React from "react";
// const React = require("react");

const fs = require("fs");
const path = require("path");

//writeFile para escribir datos sobre un archivo
const pathFile = path.resolve("numero_secreto.txt"); //Ruta del archivo absoluta
const data = "2131231"; //Datos que queremos escribir
//(error) => callback con el objeto error en caso de que haya uno

fs.writeFile(pathFile, data, (error) => {
    if(error){
        return console.log(error);
    }
    return console.log("Se ha escrito el numero secreto sobre el archivo");
});

//Error -> si sucede un error al leer el archivo
//Data -> Datos del archivo en chunks
fs.readFile(pathFile, (error, data) => {
    if(error){
        return console.log(error);
    }
    return console.log(data.toString());
});

//Error -> si sucede un error al leer el archivo
//Data -> Datos del archivo en chunks
fs.readFile(pathFile, "UTF-8", (error, data) => {
    if(error){
        return console.log(error);
    }
    return console.log(data);
});